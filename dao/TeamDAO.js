const Team = require('../models/Team').Team;

const TeamDAO = {
    getAll: async () => {
        try {
            return await Team.find({});
        } catch (err) {
            console.error(err);
        }
    },
    getByName: async (name) => {
        try {
            return await Team.find({'name': name});
        } catch (err) {
            console.error(err);
        }
    },
    add: async(team) => {
        try {
            return await Team.findOneAndUpdate(team, team, {upsert: true, new: true});
        } catch (err) {
            console.error(err);
        }
    }
}
module.exports = TeamDAO;