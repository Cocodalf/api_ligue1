const Game = require('../models/Game').Game;

const GameDAO = {
    async getAll() {
        try  {
            return await Game.find({});
        } catch (err) {
            console.error(err);
        }
    },
    async add(game) {
        try {
            return await Game.findOneAndUpdate({
                teams: game.teams
            } , game, {upsert: true, new: true});
        } catch (err) {
            console.error(err);
        }
    },
    async getById(id) {
        try {
            return await Game.find({'_id': id});
        } catch (err) {
            console.error(err);
        }
    },

}

module.exports = GameDAO;
