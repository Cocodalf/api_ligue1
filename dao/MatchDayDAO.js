const MatchDay = require('../models/MatchDay').MatchDay;

const MatchDayDAO = {
    getAll: async () => {
        try {
            return await MatchDay.find({});
        } catch (err) {
            console.error(err);
        }
    },
    add: async (matchDay) => {
        try  {
            return await MatchDay.findOneAndUpdate({
                days: matchDay.days
            }, matchDay, {upsert: true, new: true});
        } catch (err) {
            console.error(err);
        }
    }
}

module.exports = MatchDayDAO;