const Day = require('../models/Day').Day;

const DayDAO = {
    getAll: async () => {
        try {
            return await Day.find({});
        } catch (err) {
            console.error(err);
        }
    },
    add: async (day) => {
        try {
            // doublon
            return await Day.findOneAndUpdate({date: day.date}, day, {upsert: true, new: true});
        } catch (err) {
            console.error(err);
        }
    },
    getByDate: async (date) => {
        try {
            return await Day.find({'date': date});
        } catch (err) {
            console.error(err);
        }
    }
}

module.exports = DayDAO;