const htmlToJson = require('html-to-json');

const teamParser = htmlToJson.createParser(['.team', ($team) => {
    return {
        logo: $team.find('img').attr('data-isg-lazy'),
        name: $team.find('.team__label').text(),
        position: $team.find('.team__position').text()
    }
}]);

const scoreParser = htmlToJson.createParser(['.match__score-text', ($score) => {
    return $score.text()
}]);

const gameParser = htmlToJson.createParser(['div', ($div) => {
    if ($div.attr('class') === 'date-caption') {
        return $div.text();
    } else if ($div.attr('class') === 'match') {
        let game = {};
        teamParser.parse($div).then((teams) => {
            game.teams = teams;
        });
        scoreParser.parse($div).then((scores) => {
            game.scores = scores;
        });
        return game;
    }
}]);

const eurosportParser = async (html) => {
    try {
        let data = await htmlToJson.parse(html, {
            matchDay: ($doc) => { return gameParser.parse($doc) }
        });
        let matchDay = {days: []};
        let day = {date: '', games: []};
        for (let i = 0; i < data.matchDay.length; i++) {
            if (data.matchDay[i] !== undefined) { // item exist
                if (!data.matchDay[i].teams) { // its a date
                    if (day.games.length) { // need to push
                        matchDay.days.push(day);
                        day = {date: data.matchDay[i], games: []};
                    } else {
                        day.date = data.matchDay[i]; // add date
                    }
                } else {
                    day.games.push(data.matchDay[i]); // add games
                }
            } else if (i === data.matchDay.length - 1) { // last item, need to push
                matchDay.days.push(day);
            }
        }
        return matchDay;
    } catch (err) {
        return err;
    }
};


module.exports = eurosportParser;