const { uniqBy } = require('lodash');
const fs = require('fs');
const path = require('path');

const TeamDAO = require('../dao/TeamDAO');
const GameDAO = require('../dao/GameDAO');
const DayDAO = require('../dao/DayDAO');
const MatchDayDAO = require('../dao/MatchDayDAO');


class Seeder {
    constructor() {
        this.teams = [];
        this.games = [];
        this.days = [];
        this.matchDays = [];
        this.leagues = [];
    }


    async extractMatchDays() {
        for (const league of this.leagues) {
            for (const matchDay of league.matchDays) {
                this.matchDays.push(matchDay);
            }
        }
    }

    async extractDays() {
        try {
            for (const matchDay of this.matchDays) {
                for (const day of matchDay.days) {
                    this.days.push(day);
                }
            }
        } catch (err) {
            console.error(err);
        }
    }

    async extractGames() {
        try {
            for (const day of this.days) {
                for (const game of day.games) {
                    this.games.push(game);
                }
            }
        } catch (err) {
            console.error(err);
        }
    }


    async extractTeams() {
        try {
            for (const game of this.games) {
                for (const team of game.teams) {
                    this.teams.push(team);
                }
            }
        } catch (err) {
            console.error(err);
        }
    }
    async seedLLeague() {
        try {
            for (const league of this.leagues) {
                LeagueDAO.add(league);
            }
        } catch (err) {
            console.error(err);
        }
    }

    async seedMatchDays() {
        try {
            for (const matchDay of this.matchDays) {
                await MatchDayDAO.add(matchDay);
            }
            return true;
        } catch (err) {
            console.error(err);
        }
    }

    async seedDays() {
        try {
            for (const day of this.days) {
                await DayDAO.add(day);
            }
            return true;
        } catch (err) {
            console.error(err);
        }
    }
    async seedGames() {
        try {
            for (const game of this.games) {
                await GameDAO.add(game);
            }
        } catch (err) {
            console.error(err);
        }
    }
    async seedTeams() {
        try {
            for (const team of this.teams) {
                await TeamDAO.add(team);
            }
        } catch (err) {
            console.error(err);
        }
    }
    async seed(leagues) {
        try {
            this.leagues = leagues;
            await this.extractMatchDays();
            await this.extractDays();
            await this.extractGames();
            await this.extractTeams();

            await this.seedMatchDays();
            await this.seedDays();
            await this.seedGames();
            await this.seedTeams();
        } catch (err) {
            console.error(err);
        }
    }
}

module.exports = Seeder;