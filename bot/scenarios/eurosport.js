
const eurosportParser = require('../parsers/eurosportParser');

const eurosport = async (bot) => {
    try {
        const numberOfLeague = 2;
        const numberOfPage = 38;
        const results = [];
        for (let i = 0; i < numberOfLeague; i++) {
            const start = `https://www.eurosport.fr/football/ligue-${(i + 1)}/calendar-result.shtml`;
            await bot.goTo(start);
            const league = { matchDays: [] };
            for (let j = 0; j < numberOfPage; j++) {
                await bot.click('div[id="results-match-nav"]');
                await bot.click('div[data-round-id="' + (5170 + (j + 1)) + '"]');
                await bot.wait(250);
                await bot.wait('.ajax-container');
                let htmlResults = await bot.getOneHTMLContentBySelector('.ajax-container');
                league.matchDays.push(await eurosportParser(htmlResults));
            }
            results.push(league);
        }
        return results;
    } catch (err) {
        console.error(err);
    }
}

module.exports = eurosport;