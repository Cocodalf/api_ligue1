const Nightmare = require('nightmare');

/**
 * @description Class bot, he can go to a destination url, scrap eurosport league pages and parse this html data in usable data
 */
class Bot {
    /**
     * @description start a Nightmare instance
     * @param {boolean} isShowing If you want to show the bot navigation pass this to true
     */
    constructor(isShowing) {
        this.isShowing = isShowing || false;
        this.nightmare = new Nightmare({
            show: isShowing
        });
    }

    /**
     * @description bot go to url
     * @param {String} start 
     */
    async goTo(start) {
        try {
            await this.nightmare
                .goto(start);
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * @description kill the bot
     */
    async finish() {
        try {
            await this.nightmare
                .end();
        } catch (err) {
            console.error(err);
        }
    }

    async click(selector) {
        try {
            return await this.nightmare
                .click(selector);
        } catch (err) {
            console.error(err);
        }
    }

    async wait(ms) {
        try {
            await this.nightmare
                .wait(ms);
        } catch (err) {
            console.error(err);
        }
    }
    async getManyHTMLContentsBySelector(selector) {
        try {
            return await this.nightmare
                .wait(selector)
                .evaluate((selector) => {
                    return [...document.querySelectorAll(selector)]
                        .map(el => el.innerHTML);
                }, selector);
        } catch (err) {
            console.error(err);
        }
    }

    async getOneHTMLContentBySelector(selector) {
        try {
            return await this.nightmare
                .wait(selector)
                .evaluate((selector) => {
                    return document.querySelector(selector).innerHTML;
                }, selector);
        } catch (err) {
            console.error(err);
        }
    }

}

module.exports = Bot;
