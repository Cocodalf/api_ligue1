const express = require('express');

const TeamDAO = require('../../dao/TeamDAO');
const GameDAO = require('../../dao/GameDAO');
const DayDAO = require('../../dao/DayDAO');
const MatchDayDAO = require('../../dao/MatchDayDAO');

const Log = require('../../utils/Log');
const log = new Log();

const router = express.Router();


router.get('/teams', async (req, res) => {
  log.get('/teams');
  try {
    const result = await TeamDAO.getAll();
    res.status(200)
      .json(result);
  } catch (err) {
    res.status(500)
      .json(err);
  }
});

router.get('/teams/game/:gameId', async (req, res) => {
  try {
    log.get('/teams/:gameId');
    const id = req.params.gameId;
    let games = await GameDAO.getById(id);
    if (games.length) {
      res.status(200).json(game[0].teams);
    } else {
      res.status(202).json('no game with this id founded!');
    }
  } catch (err) {
    res.status(500)
    .json(err);
  }
});

router.get('/teams/name/:name', async (req, res) => {
  try {
    const name = req.params.name;
    const teams = await TeamDAO.getByName(name);
    if (teams.length) {
      res.status(200).json(teams[0]);
    } else {
      res.status(202).json('Not teams with this name founded!');
    }
  } catch (err) {
    res.status(500)
    .json(err);
  }
});


router.get('/games', async (req, res) => {
    log.get('/games');
    try {
      const result = await GameDAO.getAll();
      res.status(200)
        .json(result);
    } catch (err) {
      res.status(500)
        .json(err);
    }
});

router.get('/games/date/:date', async (req, res) => {
  log.get('/games/date/:date');
  try {
    const date = req.params.date;
    const days = await DayDAO.getByDate(date);
    if (days.length) {
      let games = [];
      for (const day of days) {
        for (const game of day.games) {
          games.push(game);
        }
      }
      res.status(200).json(games);
    } else {
      res.status(202).json('No games with this date founded!');
    }
  } catch (err) {

  }
});




router.get('/days', async (req, res) => {
  log.get('/days');
  try {
    const result = await DayDAO.getAll();
    res.status(200)
    .json(result);
  } catch (err) {
    res.status(500)
    .json(err);
  }
});

router.get('/days/date/:date', async (req, res) => {
  try {
    const date = req.params.date;
    const days = await DayDAO.getByDate(date);
    if (days.length) {
      res.status(200).json(days);
    } else {
      res.status(202).json('No days with this date founded!');
    }
  } catch (err) {
    res.status(500)
    .json(err);
  }
});

router.get('/matchDays', async (req, res) => {
  log.get('/matchDays');
  try {
    const result = await MatchDayDAO.getAll();
    res.status(200)
      .json(result);
  } catch (err) {
    res.status(500)
      .json(err);
  }
});

router.get('/matchDays/league/:leagueNumber', async (req, res) => {
  log.get('/matchDays/league/:leagueNumber');
  try {
    const leagueNumber = req.params.leagueNumber;
    const matchDays = await MatchDayDAO.getAll();
    if (leagueNumber === '1') {
      res.status(200).json(matchDays.slice(0, 37));
    } else {
      res.status(200).json(matchDays.slice(37, matchDays.length - 1));
    }
  } catch (err) {
    res.status(500)
    .json(err);
  }
});



module.exports = router;
