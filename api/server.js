#!/usr/bin/env node

const http = require('http');

const app = require('./app');

const Log = require('../utils/Log');
const log = new Log();

const start = async () => {
    try {
        const port = await normalizePort(process.env.PORT || '3000');
        app.set('port', port);
        const server = await http.createServer(app);
        server.listen(port);
        server.on('error', onError);
        server.on('listening', onListening);
        async function normalizePort(val){
          var port = parseInt(val, 10);
          if (isNaN(port)) {
            return val;
          }
          if (port >= 0) {
            return port;
          }
          return false;
        }
        function onError(error) {
          if (error.syscall !== 'listen') {
            throw error;
          }
          var bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;
          switch (error.code) {
            case 'EACCES':
              console.error(bind + ' requires elevated privileges');
              process.exit(1);
              break;
            case 'EADDRINUSE':
              console.error(bind + ' is already in use');
              process.exit(1);
              break;
            default:
              throw error;
          }
        }
        function onListening() {
          var addr = server.address();
          var bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;
          log.info('Listening on ' + bind);
        }
    } catch (err) {
        console.error(err);
    }
}

module.exports = {
    start
}