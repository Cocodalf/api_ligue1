class Log {
    constructor() {
        this.reset = "\x1b[0m";
        this.bright = "\x1b[1m";
        this.dim = "\x1b[2m";
        this.underscore = "\x1b[4m";
        this.blink = "\x1b[5m";
        this.reverse = "\x1b[7m";
        this.hidden = "\x1b[8m";
        this.fgBlack = "\x1b[30m";
        this.fgRed = "\x1b[31m";
        this.fgGreen = "\x1b[32m";
        this.fgYellow = "\x1b[33m";
        this.fgBlue = "\x1b[34m";
        this.fgMagenta = "\x1b[35m";
        this.fgCyan = "\x1b[36m";
        this.fgWhite = "\x1b[37m";
        this.bgBlack = "\x1b[40m";
        this.bgRed = "\x1b[41m";
        this.bgGreen = "\x1b[42m";
        this.bgYellow = "\x1b[43m";
        this.bgBlue = "\x1b[44m";
        this.bgMagenta = "\x1b[45m";
        this.bgCyan = "\x1b[46m";
        this.bgWhite = "\x1b[47m";
    }

    get Instance() {
        return this.instance || (this.instance = new Log());
    }

    info(info) {
        const msg =  info || '';
        console.log(this.fgGreen, "[INFO] " + msg);
        this.resetConsole();
    }

    error(error) {
        console.log(this.fgRed, "[ERROR] " + JSON.stringify(error));
        this.resetConsole();
    }

    post(post) {
        const msg =  post || '';
        console.log(this.fgBlue, "[POST] " + msg);
        this.resetConsole();
    }

    get(get) {
        const msg =  get || '';
        console.log(this.fgYellow, "[GET] " + msg);
        this.resetConsole();
    }

    put(put) {
        const msg =  put || '';
        console.log(this.fgCyan, "[UPDATE] " + msg);
        this.resetConsole();
    }

    delete(del) {
        const msg =  del || '';
        console.log(this.fgMagenta, "[DELETE] " + msg);
        this.resetConsole();
    }

    resetConsole() {
        console.log(this.reset);
    }
}

module.exports = Log;
