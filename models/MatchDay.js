const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DaySchema = require('./Day').DaySchema;

const MatchDaySchema = new Schema({
    days: [DaySchema]
});

const MatchDay = mongoose.model('MatchDay', MatchDaySchema);

module.exports = {
    MatchDaySchema,
    MatchDay
}