const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TeamSchema = new Schema({
    name: String,
    logo: String,
    position: String
});

const Team = mongoose.model('Team', TeamSchema);

module.exports = {
    TeamSchema,
    Team,
}