const mongoose = require('mongoose');

const GameSchema = require('./Game').GameSchema;

const Schema = mongoose.Schema;

const DaySchema = new Schema({
    date: String,
    games: [GameSchema]
});

const Day = mongoose.model('Day', DaySchema);

module.exports = {
    DaySchema,
    Day
}