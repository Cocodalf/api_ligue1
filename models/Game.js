
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TeamSchema = require('./Team').TeamSchema;

const GameSchema = new Schema({
    scores: [String],
    teams: [TeamSchema]
});


const Game = mongoose.model('Game', GameSchema);

module.exports = {
    GameSchema,
    Game
}