# Eurosport API

Bienvenue dans notre api Eurosport qui va vous permettre de récupérer en direct toutes les ligues, matches, journée de matches ...


## Installation:
### Télécharger et installer NodeJS:
[NodeJS Download](https://nodejs.org/en/download/)
###  Télécharger et installer MongoDB:
[Mongodb for windows](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)
 [Mongodb for Linux](https://docs.mongodb.com/manual/administration/install-on-linux/)
[Mongodb for McOs](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/)

### Télécharger le projet et installer les dépendances:

    $ git clone https://gitlab.com/Cocodalf/api_ligue1.git
    cd api_ligue1
    npm install

## Lancer l'API:
Lancer MongoDB puis:
	
    cd api_ligue1
    npm start

Attendre que la console affiche:

    [INFO] Listening on port 3000


## Déroulement:

1) Un client MongoDB va se connecter à la base de données.
2) Le bot va allé scrapper les pages du site d'[Eurosport](https://www.eurosport.fr/football/ligue-1/) et parser les données.
3) Le "Seeder" va alimenter la base de donnée avec les datas fraîchement récupéré.
4) L'api se lance et permet l'utilisation de ces routes.
## API routes:


### Teams:
- Get all teams:
	- url: localhost:3000/teams/
	- return: JSON of all the teams.
	- require: nothing.
- Get teams by game id:
	- url: localhost:3000/teams/game/:gameId/
	- return: JSON of all the teams in the games getted by id in params.
	-  require: params gameId (string, ObjectId).
- Get team by name:
	- url: localhost:3000/teams/name/:name/
	- return: JSON of the team getted by name.
	- require: params name (string).

----------

### Games:
- Get all games:
	- url: localhost:3000/games/
	- return: JSON of all the games.
	- require: nothing
- Get games by date:
	- url: localhost:3000/games/date/:date/
	- return: JSON of all games getted by date.
	- require: params date (string).


----------

### Days:
- Get all the days:
	- url: localhost:3000/days/
	- return: JSON of all days.
	- require: nothing.
- Get days by date:
	- url: localhost:3000/days/date/:date/
	- return: JSON of all the days getted by date.
	- require: params date (string).


----------
### Match days:

- Get all the match days:
	- url: localhost:3000/matchDays/
	- return: JSON of all match days.
	- require: nothing.
- Get match days by league number (or get league):
	- url: localhost:3000/matchDays/league/:leagueNumber/
	- return: JSON of league getted by number.
	- require: params leagueNumber (number).

