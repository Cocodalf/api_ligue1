const mongoose = require('mongoose');
const mongoConfig = require('../config/db').mongo;
const Log = require('../utils/Log');
const log = new Log();

class MongoClient {
    constructor() {
    }

    async connect() {
        try {
            await mongoose.connect(`mongodb://${mongoConfig.host}:${mongoConfig.port}/${mongoConfig.database}`);
            log.info('Mongodb successfully connected!');
            mongoose.Promise = global.Promise;
            this.db = mongoose.connection;
            this.db.db.dropDatabase();
            this.db.on('error', () => {
                log.error('Mongodb connection error!');
            });
            process.on('SIGINT', () => {
                this.db.close(() => {
                    log.info('Mongoose default connection disconnected!');
                    process.exit(0);
                });
            });
            return true;
        } catch (err) {
            console.error(err);
        }
    }
}


module.exports = MongoClient;