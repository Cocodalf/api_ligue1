const dbConfig = {
    mongo: {
        host: 'localhost',
        port: '27017',
        database: 'eurosport_league'
    }
}

module.exports = dbConfig;