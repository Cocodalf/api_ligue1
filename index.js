const fs = require('fs');
const path = require('path');

const MongoClient = require('./db/MongoClient');

const Bot = require('./bot/Bot');
const scenarios = require('./bot/scenarios');

const Seeder = require('./bot/Seeder');

const Api = require('./api');

const Log = require('./utils/Log');

/**
 * @description Entry point
 * * 1) Create Mongoose instance and connect to db
 * * 2) Create Bot instance and go scrap the two leagues in `https://www.eurosport.fr/football/ligue-${leagueNumber}/calendar-result.shtml`;
 * * 3) Create Seeder instance and save or update all data in db
 * * 4) Start Api
 */
async function main() {
    const log = new Log();
    log.info('START!');
    let leagues;
    try {
        // Connect to DB
        log.info('Connect to MongoDB...');
        const mongoClient = new MongoClient();
        await mongoClient.connect();

        const bot = new Bot(true);
        let leagues = await scenarios.eurosport(bot);
        await bot.finish();
        
        const seeder = new Seeder();
        await seeder.seed(leagues);

        Api.start();

        log.info('DONE')

    } catch (err) {
        console.error(err);
    }
}

main()
    .catch(err => {
        console.error(err);
    })